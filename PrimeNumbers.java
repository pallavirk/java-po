package com.java.core;

import java.util.Scanner;

public class PrimeNumbers {

	public static void main(String args[]) {
		int i, m = 0, flag = 0;
		
		System.out.println("Enter the number to check whether it is prime or not");
		Scanner sc =new Scanner(System.in);
		int n=sc.nextInt();
		m = n / 2;
		int k=0;
		if (n == 0 || n == 1) {
			System.out.println(n + " is not prime number");
		} else {
			for (i = 2; i <n; i++) {
				if (n % i == 0) {
					System.out.println(n + " is not prime number");
					k++;
					flag = 1;
					break;
				}
			}
			
			if (flag == 0) {
				System.out.println(n + " is prime number");
			}
		} // end of else
		System.out.println("no of times iterated"+k);

	}
}
